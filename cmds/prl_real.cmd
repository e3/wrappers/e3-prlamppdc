# PRL
# 2 Connections one for SPI and one for I2C

require xtpico, develop

# Prefix for all records
epicsEnvSet("P",           "TEST:")
epicsEnvSet("DEVICE_IP",        "192.168.100.100")

# For amplifiers
epicsEnvSet("SPI_COMM_PORT",    "AK_SPI_COMM")
# For PDC
epicsEnvSet("I2C_COMM_PORT",    "AK_I2C_COMM")

#- Create the asyn port to talk XTpico server on TCP port 
drvAsynIPPortConfigure($(SPI_COMM_PORT),"$(DEVICE_IP):1002")
drvAsynIPPortConfigure($(I2C_COMM_PORT),"$(DEVICE_IP):1003")

#asynSetTraceIOMask("$(I2C_COMM_PORT)",0,255)
#asynSetTraceMask("$(I2C_COMM_PORT)",0,255)

PRLAmpPDCConfigure("PRL", "$(SPI_COMM_PORT)", "$(I2C_COMM_PORT)", 0x32, 0, 0)
dbLoadRecords("PRL_Amp_PDC.db", "P=$(P),R=,PORT=PRL,ADDR=0,TIMEOUT=1")
#asynSetTraceIOMask("PRLHPA.$(N)",0,255)
#asynSetTraceMask("PRLHPA.$(N)",0,255)
