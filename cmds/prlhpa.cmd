# PRLHPA

require xtpico, develop

# Prefix for all records
epicsEnvSet("P",           "TEST:")
epicsEnvSet("DEVICE_IP",        "172.30.6.90")

epicsEnvSet("SPI_COMM_PORT1",    "AK_SPI_COMM1")
epicsEnvSet("SPI_COMM_PORT2",    "AK_SPI_COMM2")


#- Create the asyn port to talk XTpico server on TCP port 1002
drvAsynIPPortConfigure($(SPI_COMM_PORT1),"$(DEVICE_IP):1002")
#- Create the asyn port to talk XTpico server on TCP port 1003 for SPI2
drvAsynIPPortConfigure($(SPI_COMM_PORT2),"$(DEVICE_IP):1003")
#asynSetTraceIOMask("$(SPI_COMM_PORT)",0,255)
#asynSetTraceMask("$(SPI_COMM_PORT)",0,255)


iocshLoad("$(xtpico_DIR)/prlhpa.iocsh", "IP_PORT=$(SPI_COMM_PORT1), N=1, R=HPA352:")
iocshLoad("$(xtpico_DIR)/prlhpa.iocsh", "IP_PORT=$(SPI_COMM_PORT2), N=2, R=HPA704:")
