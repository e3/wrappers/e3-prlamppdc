# PRLHPA

require xtpico, develop

# Prefix for all records
epicsEnvSet("P",           "TEST:")
epicsEnvSet("DEVICE_IP",        "127.0.0.1")

epicsEnvSet("I2C_COMM_PORT",    "AK_I2C_COMM")


#- Create the asyn port to talk XTpico server on TCP port 1002
drvAsynIPPortConfigure($(I2C_COMM_PORT),"$(DEVICE_IP):9999")

#asynSetTraceIOMask("$(I2C_COMM_PORT)",0,255)
#asynSetTraceMask("$(I2C_COMM_PORT)",0,255)

AKI2CPRLPDCConfigure("PRLPDC", "$(I2C_COMM_PORT)", 0, 0)
dbLoadRecords("AKI2C_PRLPDC.db", "P=$(P),R=,PORT=PRLPDC,IP_PORT=$(I2C_COMM_PORT),ADDR=0,TIMEOUT=1")
#asynSetTraceIOMask("PRLHPA.$(N)",0,255)
#asynSetTraceMask("PRLHPA.$(N)",0,255)

