#
#  Copyright (c) 2019    European Spallation Source ERIC
#
#  The program is free software: you can redistribute
#  it and/or modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation, either version 2 of the
#  License, or any newer version.
#
#  This program is distributed in the hope that it will be useful, but WITHOUT
#  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
#  more details.
#
#  You should have received a copy of the GNU General Public License along with
#  this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
#
# 
# this module was based on the xtpico module
# 

## The following lines are mandatory, please don't change them.
where_am_I := $(dir $(abspath $(lastword $(MAKEFILE_LIST))))
include $(E3_REQUIRE_TOOLS)/driver.makefile




## Exclude linux-ppc64e6500
##EXCLUDE_ARCHS += linux-ppc64e6500
##EXCLUDE_ARCHS += linux-corei7-poky

APP:=xtpicoApp
APPDB:=$(APP)/Db
APPSRC:=$(APP)/src


USR_INCLUDES += -I$(where_am_I)$(APPSRC)

# USR_CFLAGS   += -Wno-unused-variable
# USR_CFLAGS   += -Wno-unused-function
# USR_CFLAGS   += -Wno-unused-but-set-variable
# USR_CPPFLAGS += -Wno-unused-variable
# USR_CPPFLAGS += -Wno-unused-function
# USR_CPPFLAGS += -Wno-unused-but-set-variable

# For Debug
#USR_CPPFLAGS += -D_DBG=1

TEMPLATES += $(wildcard $(APPDB)/*.db)


# DBDINC_SRCS += $(APPSRC)/swaitRecord.c
# DBDINC_DBDS = $(subst .c,.dbd,   $(DBDINC_SRCS:$(APPSRC)/%=%))
# DBDINC_HDRS = $(subst .c,.h,     $(DBDINC_SRCS:$(APPSRC)/%=%))
# DBDINC_DEPS = $(subst .c,$(DEP), $(DBDINC_SRCS:$(APPSRC)/%=%))


HEADERS += $(wildcard $(APPSRC)/*.h)
# HEADERS += $(DBDINC_HDRS)

SOURCES += $(APPSRC)/AKBase.cpp
SOURCES += $(APPSRC)/AKI2C.cpp
SOURCES += $(APPSRC)/PRL_Amp_PDC.cpp

# # DBDINC_SRCS should be last of the series of SOURCES
# SOURCES += $(DBDINC_SRCS)

DBDS += $(APPSRC)/xtpicoSupport.dbd


SCRIPTS += $(wildcard ../iocsh/*.iocsh)


## This RULE should be used in case of inflating DB files 
## db rule is the default in RULES_DB, so add the empty one
## Please look at e3-mrfioc2 for example.

# # .PHONY: 

#
USR_DBFLAGS += -I . -I ..
USR_DBFLAGS += -I $(EPICS_BASE)/db
USR_DBFLAGS += -I $(APPDB)
#
SUBS=$(wildcard $(APPDB)/*.substitutions)
# SUBS=$(wildcard $(APPDB)/AKTTLIO.substitutions)

# TMPS=$(wildcard $(APPDB)/*.template)
#
# 
vlibs:

.PHONY: vlibs
